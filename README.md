# Leaderboard

Leaderboard page on localhost with a scoreboard software embedded into the page using an iframe

**README***

- Open and run the "Leaderboard" html page on the TV/Screen/Monitor for display.

- On a separate screen/monitor open the link at the bottom, this will open up the master control page to modify the leaderboard.

- Don't zoom in and out rapidly as the frame rate will drop due to the resizing of the animated background. If this problem occurs, reload the page.



**PLAYER**

- To ADD a NEW PLAYER, click on the "MORE" button at the bottom of the page, then proceed to click on the "Add Player" option and fill in the new player's details.

- To DELETE an EXISTING PLAYER, click on the player's name, then proceed to click on the "DELETE PLAYER" button.

- To EDIT an EXISTING PLAYER, click on the player's name, then proceed to modify the name on the text box and click on the "SAVE. NAME" button.




**SCORE**

- To ADD NEW SCORES, click on the "ADD SCORE" button at the top of the page. A list of your players will appear with each one having a text box. Manually input the scores they got on the game and click on the "SAVE" button at the bottom.

	**WARNING: DON'T INPUT SCORES ON TOP OF EXISTING PLAYER'S SCORES...
	THE VALUE YOU PUT IN WILL BE ADDED WITH THE EXISTING SCORE...**
	
- To EDIT EXISTING SCORES, click the pencil icon on the very right of the row of scores, this will display the scores the player's got that "round" and allow you to edit the scores.

- To DELETE EXISTING SCORES, click the pencil icon on the very right of the row of scores, delete the score on the individual player's text box and "SAVE" OR click "DELETE ROUND" button at the very bottom of the page.


**WARNING**

LINK ONLY FOR ADMIN, DON'T GIVE ACCESS TO USERS
http://keepthescore.co/game/mjoyanrqiae/